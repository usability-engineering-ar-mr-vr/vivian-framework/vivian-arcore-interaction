#if UNITY_EDITOR && !UNITY_2020_1_OR_NEWER

using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class CreateGradleConfiguration:IPreprocessBuildWithReport
{
    // Trigger the preprocessing method before ARCore's by setting a lower callBackOrder (ARCore is set at 0)
    public int callbackOrder => -1;

    public void OnPreprocessBuild(BuildReport report)
    {
        if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android)
        {
            if (!AssetDatabase.IsValidFolder("Assets/Plugins")) {
                AssetDatabase.CreateFolder("Assets", "Plugins");
            }

            if (!AssetDatabase.IsValidFolder("Assets/Plugins/Android")) {
                AssetDatabase.CreateFolder("Assets/Plugins", "Android");
            }

            #if UNITY_2019_3_OR_NEWER
                if(!File.Exists(Application.dataPath + "/Plugins/Android/launcherTemplate.gradle"))
                {
                    // Remove "/Assets" from the path
                    File.Copy(Application.dataPath.Substring(0, Application.dataPath.Length-7) 
                              + "/Packages/vivian-arcore-interaction/Editor/Templates/launcherTemplate.gradle", 
                        Application.dataPath + "/Plugins/Android/launcherTemplate.gradle");
                }
                if (!File.Exists(Application.dataPath + "/Plugins/Android/mainTemplate.gradle"))
                {
                    // Remove "/Assets" from the path
                    File.Copy(Application.dataPath.Substring(0, Application.dataPath.Length - 7)
                              + "/Packages/vivian-arcore-interaction/Editor/Templates/mainTemplateApi30.gradle",
                        Application.dataPath + "/Plugins/Android/mainTemplate.gradle");
                }
            #endif

            #if UNITY_2019_1_OR_NEWER
                if (!File.Exists(Application.dataPath + "/Plugins/Android/mainTemplate.gradle"))
                {
                    // Remove "/Assets" from the path
                    File.Copy(Application.dataPath.Substring(0, Application.dataPath.Length - 7)
                              + "/Packages/vivian-arcore-interaction/Editor/Templates/mainTemplate.gradle",
                              Application.dataPath + "/Plugins/Android/mainTemplate.gradle");
                }
            #endif
        }
    }
}
#endif
﻿using System;
using System.Collections.Generic;
using de.ugoe.cs.vivian.core;
using UnityEngine;
using UnityEngine.UI;

public class AppController : MonoBehaviour
{
    public Button returnButtonGameObject;
    public GameObject menuCanvas;
    public GameObject appCanvas;
    public GameObject vivianFrameworkPrefab;
    public ObjectPositioner objectPositioner;
    private VirtualPrototype virtualPrototype;

    [Serializable]
    public struct PrototypeUrls
    {
        public string bundleURL;
        public string prototypePrefabName;
        public Button buttonGameObject;
    };
    public List<PrototypeUrls> prototypeUrls;
    
    void Start()
    {
        // Add listeners to the buttons
        foreach (PrototypeUrls pUrl in prototypeUrls)
        {
            pUrl.buttonGameObject.onClick.AddListener(() => LoadPrototype(pUrl.bundleURL, pUrl.prototypePrefabName));
        }
        returnButtonGameObject.onClick.AddListener(() => ToggleMenu(true));
    }

    private void ToggleMenu(bool bToggle)
    {
        this.appCanvas.SetActive(!bToggle);
        this.menuCanvas.SetActive(bToggle);
    }

    private void LoadPrototype(string url, string name)
    {
        // Hide menu
        ToggleMenu(false);

        if (virtualPrototype != null && virtualPrototype.BundleURL != url)
        {
            Destroy(virtualPrototype.gameObject);
            InstantiateVP(url, name);
        }
        else if(virtualPrototype == null)
            InstantiateVP(url, name);
    }

    private void InstantiateVP(string url, string name)
    {
        virtualPrototype = Instantiate(vivianFrameworkPrefab).GetComponent<VirtualPrototype>();
        virtualPrototype.BundleURL = url;
        virtualPrototype.PrototypePrefabName = name;
        objectPositioner.ResetVirtualPrototype(virtualPrototype);
    }
}

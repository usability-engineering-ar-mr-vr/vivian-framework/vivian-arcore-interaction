﻿// Copyright 2019 Patrick Harms
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace de.ugoe.cs.vivian.arcoreinteraction
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using de.ugoe.cs.vivian.core;

#if UNITY_EDITOR
    // Set up touch input propagation while using Instant Preview in the editor.
    using Input = GoogleARCore.InstantPreviewInput;
#endif

    /**
     * controller for AR Core based interaction
     */
    public class ARController : MonoBehaviour
    {
        // the prototypes in the scene while interacting
        private VirtualPrototype[] Prototypes = null;

        /**
         * called on awake to set the target framerate
         */
        public void Awake()
        {
            // Enable ARCore to target 60fps camera capture frame rate on supported devices.
            // Note, Application.targetFrameRate is ignored when QualitySettings.vSyncCount != 0.
            Application.targetFrameRate = 60;
        }

        /**
         * Called every frame to check for touches
         */
        public void Update()
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);

                // Should not handle input if the player is pointing on UI.
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    return;
                }

                if (touch.phase == TouchPhase.Began)
                {
                    HandleTouchBegan(touch);
                }
                else if ((touch.phase == TouchPhase.Moved) || (touch.phase == TouchPhase.Stationary))
                {
                    // we also handle stationary touches as the user may have moved the phone
                    HandleTouchMoved(touch);
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    HandleTouchEnded(touch);
                }
            }
        }

        /**
         * handles a touch begin
         */
        private void HandleTouchBegan(Touch touch)
        {
            this.Prototypes = FindObjectsOfType<VirtualPrototype>();

            if (this.Prototypes != null)
            {
                Pose pose = this.GetPose(touch);

                foreach (VirtualPrototype prototype in this.Prototypes)
                {
                    prototype.TriggerInteractionStarts(pose, touch.fingerId);
                }
            }
        }

        /**
         * handles a touch move
         */
        private void HandleTouchMoved(Touch touch)
        {
            if (this.Prototypes != null)
            {
                Pose pose = this.GetPose(touch);

                foreach (VirtualPrototype prototype in this.Prototypes)
                {
                    prototype.TriggerInteractionContinues(pose, touch.fingerId);
                }
            }
        }

        /**
         * handles a touch end
         */
        private void HandleTouchEnded(Touch touch)
        {
            if (this.Prototypes != null)
            {
                Pose pose = this.GetPose(touch);

                foreach (VirtualPrototype prototype in this.Prototypes)
                {
                    prototype.TriggerInteractionEnds(pose, touch.fingerId);
                }
            }
        }

        /**
         * convenience method to determine the pose of interaction
         */
        private Pose GetPose(Touch touch)
        {
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            return new Pose(ray.origin, Quaternion.LookRotation(ray.direction, Camera.main.transform.up));
        }
    }
}


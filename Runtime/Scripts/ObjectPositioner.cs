﻿// Copyright 2019 Patrick Harms
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System.Collections;
using System.Collections.Generic;
using de.ugoe.cs.vivian.core;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using GoogleARCore.Examples.ObjectManipulation;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
// Set up touch input propagation while using Instant Preview in the editor.
using Input = GoogleARCore.InstantPreviewInput;
#endif

/**
 * Inits a game object directly in front of the camera or on a touch on the display.
 * In the second case, the object is initially deactivated. Then, on a touch of the user,
 * an anchor right in front of the display is created and the object is attached to it.
 * Finally, the object is activated and the game object to which this script is attached
 * is deactivated.
 */
public class ObjectPositioner : MonoBehaviour {

    // The game object to position
    public GameObject gameObjectToPosition;

    /** */
    public bool autoPositionAtStart = false;

    /** */
    public bool withAnchor = true;

    // Transparent material applied to the preview prefab 
    public Material PreviewMaterial;

    // Contains the original materials for the VP
    private List<Material> lstOriginalMaterials;
    
    // Manipulator prefab to attach placed objects to.
    public GameObject manipulatorPrefab;
    
    // Manipulator GameObject created from the prefab
    private GameObject manipulator;

    // Manipulation System
    private GameObject manipulationSystem;
    
    // The preview position validation button to instantiate final VP in "plane" mode
    public GameObject validatePreviewButton;    
    
    // The manipulation button to go back to manipulation mode
    public GameObject manipulationButton;

    // The VP component taken from gameObjectToPosition
    private VirtualPrototype virtualPrototype;

    // Flag that detects if the user touches the screen for the 1st time, for "plane" mode
    private bool firstTouch = true;

    // The TouchKit GameObject in the Scene to be activated after validation in "plane" mode
    public GameObject touchKit;

    public enum PositionEnum {
        relativeToScreen,
        relativeToFloor,
        relativeToPlane
    }
    
    // List of positioning modes in the hierarchy
    public PositionEnum PositionMode = PositionEnum.relativeToPlane;

    void Start () {
        // deactivate VP in case the object is active in the hierarchy
        if(gameObjectToPosition != null)
            gameObjectToPosition.SetActive(false);
        
        // If the positioning mode is set to "plane", the VP will be instantiated by this script and not by the object's script
        // In "plane" mode we only instantiate when touching, so activating the VP script will start fetching resources
        if (PositionMode == PositionEnum.relativeToPlane)
        {
            // The VivianFramework GameObject can be instantiated by the AppController
            // In this case, there is no VF GO in the hierarchy to link to gameObjectToPosition
            if (gameObjectToPosition != null)
            {
                this.virtualPrototype = gameObjectToPosition.GetComponent<VirtualPrototype>();
                this.virtualPrototype.InstantiatedByObjectPositioner = true;
            }
            manipulationSystem = GameObject.Find("ManipulationSystem");
        }
    }
	
	/**
     * waits for a touch of the user to position the game object depending on the chosen position
     */
	void Update () {
        // If the user has not touched the screen, we are done with this update.
        if (!autoPositionAtStart && ((Input.touchCount < 1) || ((Input.GetTouch(0)).phase != TouchPhase.Began)))
        {
            return;
        }
        
        if (PositionMode == PositionEnum.relativeToScreen)
        {
            positionRelativeToScreen();
        }
        else if (PositionMode == PositionEnum.relativeToFloor)
        {
            positionRelativeToFloor();
        }
        else if (PositionMode == PositionEnum.relativeToPlane)
        {
            // Don't deactivate the script in this case, we need to track further touches
            StartCoroutine(PositionRelativeToPlane());
        }
    }

    /**
     * positions the provided game object by creating an anchor for the object and put it
     * there. The positioning happens relative to the screen.
     */
    private void positionRelativeToScreen()
    {
        Debug.Log("registered initial touch --> positioning (relative to screen) and enabling touchscreen plane");

        Transform cameraTransform = Camera.main.transform;
        Vector3 cameraDirection = cameraTransform.forward;
        cameraDirection.Normalize();

        Debug.Log("cameraPosition: " + cameraTransform.position);
        Debug.Log("cameraDirection: " + cameraDirection);
        Vector3 anchorPosition = cameraTransform.position;

        if (withAnchor)
        {
            Anchor anchor = Session.CreateAnchor(new Pose(anchorPosition, cameraTransform.rotation));
            Debug.Log("anchorPosition: " + anchor.transform.position);
            Debug.Log("anchorDirection: " + anchor.transform.forward);

            gameObjectToPosition.transform.parent = anchor.transform;
        }

        gameObjectToPosition.transform.position = anchorPosition;
        gameObjectToPosition.transform.rotation = cameraTransform.rotation;

        Debug.Log("objectPosition: " + gameObjectToPosition.transform.position);
        Debug.Log("objectDirection: " + gameObjectToPosition.transform.forward);
        
        Debug.Log("touchscreen activate, initialization deactivated");
        this.gameObjectToPosition.SetActive(true);
        this.gameObject.SetActive(false);

    }

    /**
     * positions the provided game object by creating an anchor for the object and put it
     * there. The positioning happens relative to the floor.
     */
    private void positionRelativeToFloor()
    {
        Debug.Log("registered initial touch --> positioning (relative to floor) and enabling touchscreen plane");

        Transform cameraTransform = Camera.main.transform;
        Vector3 cameraDirection = cameraTransform.forward;
        cameraDirection.Normalize();

        Debug.Log("cameraPosition: " + cameraTransform.position);
        Debug.Log("cameraDirection: " + cameraDirection);
        Vector3 anchorPosition = cameraTransform.position;

        if (withAnchor)
        {
            Anchor anchor = Session.CreateAnchor(new Pose(anchorPosition, cameraTransform.rotation));
            Debug.Log("anchorPosition: " + anchor.transform.position);
            Debug.Log("anchorDirection: " + anchor.transform.forward);

            gameObjectToPosition.transform.parent = anchor.transform;
        }

        gameObjectToPosition.transform.position = anchorPosition;
        //gameObjectToPosition.transform.rotation = cameraTransform.rotation;
        gameObjectToPosition.transform.Rotate(new Vector3(0, -180, 0));

        Debug.Log("objectPosition: " + gameObjectToPosition.transform.position);
        Debug.Log("objectDirection: " + gameObjectToPosition.transform.forward);
        
        Debug.Log("touchscreen activate, initialization deactivated");
        this.gameObjectToPosition.SetActive(true);
        this.gameObject.SetActive(false);
    }
    
    /**
     * Positions a preview on the first touch, then the provided game object on second touch,
     * by creating an anchor on a touched detected plane. If the second touch happens on a different detected plane,
     * the preview is moved to that plane. This goes until the next touch matches the same plane as the preview.
     * The positioning happens relative to the plane.
     */
    private IEnumerator PositionRelativeToPlane()
    {
        TrackableHitFlags raycastFilter= TrackableHitFlags.PlaneWithinPolygon | TrackableHitFlags.FeaturePointWithSurfaceNormal;
        if (Frame.Raycast(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, raycastFilter, out var hit))
        {
            if (hit.Trackable is DetectedPlane)
            {
                // If this is the first touch on the screen, instantiate preview on touched plane
                if (this.firstTouch)
                {
                    Debug.Log("registered initial touch --> positioning (relative to plane) and instantiating VP in manipulation mode");
                    // Wait until VP resources are loaded
                    yield return new WaitUntil(() => virtualPrototype.IsResourceLoadingReady());
                    
                    // Create anchor on touched plane
                    Anchor anchor = hit.Trackable.CreateAnchor(hit.Pose);
                    // If creating an anchor failed, cancel the process
                    if (anchor == null) yield return null;
                    gameObjectToPosition.transform.parent = anchor.transform;
                    gameObjectToPosition.transform.SetPositionAndRotation(hit.Pose.position, Quaternion.Euler(0,-180, 0));
                    virtualPrototype.InstantiatePrototype();
                    
                    // set the VP transparent for preview
                    lstOriginalMaterials = new List<Material>();
                    foreach (MeshRenderer child in gameObjectToPosition.GetComponentsInChildren<MeshRenderer>())
                    {
                        lstOriginalMaterials.Add(child.material);
                    }
                    
                    if(manipulator == null)
                        this.manipulator = Instantiate(manipulatorPrefab);
                    else
                        manipulator.SetActive(true);
                    manipulator.transform.position = hit.Pose.position;
                    manipulator.transform.rotation = hit.Pose.rotation;
                    // Make VP a child of the manipulator
                    this.gameObjectToPosition.transform.parent = manipulator.transform;
                    // Make manipulator a child of the anchor
                    manipulator.transform.parent = anchor.transform;
                    
                    ToggleManipulation(true);

                    validatePreviewButton.GetComponent<Button>().onClick.AddListener(() => ToggleManipulation(false));
                    manipulationButton.GetComponent<Button>().onClick.AddListener(() => ToggleManipulation(true));

                    this.firstTouch = false;
                }
            }
        }
        // The user did not touch a detected plane, nothing happens so we give feedback
        /*else
        {
            AndroidToast.ShowFeedback("Touch a detected plane to create Virtual Prototype", AndroidToast.Gravity.TOP, 0, 0, true);
        }*/
    }

    private void ShowPlanes(bool bShow)
    {
        // Reactivate plane visualization
        GetComponent<DetectedPlaneGenerator>().enabled = true;
        foreach (Transform plane in transform)
        {
            plane.gameObject.SetActive(bShow);
        }
    }

    private void SwitchButtons(bool bSwitch)
    {
        this.validatePreviewButton.SetActive(bSwitch);
        this.manipulationButton.SetActive(!bSwitch);
    }

    private void ToggleManipulation(bool bToggle)
    {
        // Toggle transparency of VP
        int i = 0;
        foreach (MeshRenderer child in gameObjectToPosition.GetComponentsInChildren<MeshRenderer>())
        {
            child.material = bToggle ? PreviewMaterial : lstOriginalMaterials[i];
            i++;
        }
        
        // Toggle Manipulation System
        manipulationSystem.SetActive(bToggle);
        if (bToggle)
            manipulator.GetComponent<Manipulator>().Select();
        else
            manipulator.GetComponent<Manipulator>().Deselect();
        
        // Toggle Plane visualization
        ShowPlanes(bToggle);
        // Toggle Touch Kit
        this.touchKit.SetActive(!bToggle);
        // Switch buttons
        SwitchButtons(bToggle);
    }

    public void ResetVirtualPrototype(VirtualPrototype newVp)
    {
        gameObjectToPosition = newVp.gameObject;
        virtualPrototype = newVp;
        virtualPrototype.InstantiatedByObjectPositioner = true;
        firstTouch = true;
        // Hide the manipulator visualization
        if(manipulator != null)
            manipulator.SetActive(false);
        SwitchButtons(true);
        ShowPlanes(true);
    }
}
